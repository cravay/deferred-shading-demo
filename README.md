# deferred shading demo

This demo renders a simple scene using deferred shading. The code is based on the great tutorials on https://learnopengl.com and https://webgl2fundamentals.org.


## Used libraries
* [JavaScript Performance Monitor](https://github.com/mrdoob/stats.js)
* [TWGL: A Tiny WebGL helper Library](https://github.com/greggman/twgl.js)
