import * as twgl from "./vendor/twgl-full.module.js";
import Stats from "./vendor/stats.module.js";
import * as dat from './vendor/dat.gui.module.js';
import bunny from "./vendor/bunny-model.js";

// Initialize FPS counter
const stats = new Stats();
stats.showPanel(0);
document.body.appendChild(stats.dom);

// Initialize dat GUI
const gui = new dat.GUI();

const variables = {
    rotate: true,
    xTranslation: 0.0,
    yTranslation: 0.0, // TODO: investigate why this affects the background
    zTranslation: 0.0
};

gui.add(variables, "rotate");
gui.add(variables, "xTranslation");
gui.add(variables, "yTranslation");
gui.add(variables, "zTranslation");

// Fetch shaders
Promise.all(
    [
        fetch("shaders/g-buffer.vert"),
        fetch("shaders/g-buffer.frag"),
        fetch("shaders/deferred.vert"),
        fetch("shaders/deferred.frag")
    ].map(promise =>
        promise.then(response => response.text())
    )
).then(run);

function run([gVS, gFS, dVS, dFS]) {
    const { v3, m4 } = twgl;

    /** @type{WebGL2RenderingContext} */
    const gl = document.getElementById("canvas").getContext("webgl2");
    gl.enable(gl.DEPTH_TEST);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);

    /** @type{twgl.ProgramInfo} */
    const programInfo = twgl.createProgramInfo(gl, [gVS, gFS]);

    /** @type{twgl.ProgramInfo} */
    const programInfoDeferred = twgl.createProgramInfo(gl, [dVS, dFS]);

    twgl.resizeCanvasToDisplaySize(gl.canvas);

    // Enable extension for floats
    gl.getExtension("EXT_color_buffer_float");

    const attachments = [
        { internalFormat: gl.RGBA16F },
        { internalFormat: gl.RGBA16F },
        { format: gl.RGBA, type: gl.UNSIGNED_BYTE },
        { format: gl.DEPTH_STENCIL },
    ];

    /** @type{twgl.FramebufferInfo} */
    const fbi = twgl.createFramebufferInfo(gl, attachments, gl.canvas.width, gl.canvas.height);

    // Buffer info
    const bufferInfoBunny = twgl.createBufferInfoFromArrays(gl, bunny);
    const bufferInfoQuad = twgl.primitives.createXYQuadBufferInfo(gl);

    // Render-loop
    function render(time) {
        stats.begin();

        // Render to the framebuffer (first pass)
        gl.bindFramebuffer(gl.FRAMEBUFFER, fbi.framebuffer);

        // Set up perspective
        const fieldOfViewRadians = 60 / 180 * Math.PI;
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100;
        const perspective = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

        // First bunny
        let model = m4.identity(); // Create model
        model = m4.translate(model, v3.create(7.0, -5.0, -30.0)); // Move backwards along the z-axis
        if (variables.rotate) model = m4.rotateY(model, time * 0.001); // Rotate

        const uniforms = {
            u_time: time * 0.001,
            u_model: model,
            u_perspective: perspective,
            u_color: [0.0, 0.0, 1.0]
        };

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.useProgram(programInfo.program);
        twgl.setBuffersAndAttributes(gl, programInfo, bufferInfoBunny);
        twgl.setUniforms(programInfo, uniforms);
        gl.drawBuffers([
            gl.COLOR_ATTACHMENT0,
            gl.COLOR_ATTACHMENT1,
            gl.COLOR_ATTACHMENT2
        ]);
        twgl.drawBufferInfo(gl, bufferInfoBunny);


        // Second bunny
        model = m4.identity(); // Create model
        model = m4.translate(model, v3.create(0.0, -5.0, -20.0)); // Move backwards along the z-axis
        if (variables.rotate) model = m4.rotateY(model, time * 0.001); // Rotate

        uniforms.u_color = [1.0, 0.0, 0.0]
        uniforms.u_model = model;

        twgl.setUniforms(programInfo, uniforms);
        twgl.drawBufferInfo(gl, bufferInfoBunny);

        // Third bunny
        model = m4.identity(); // Create model
        model = m4.translate(model, v3.create(-8.0, -5.0, -30.0)); // Move backwards along the z-axis
        if (variables.rotate) model = m4.rotateY(model, time * 0.001); // Rotate

        uniforms.u_color = [0.0, 1.0, 0.0]
        uniforms.u_model = model;

        twgl.setUniforms(programInfo, uniforms);
        twgl.drawBufferInfo(gl, bufferInfoBunny);

        // Render to the canvas (second pass)
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        twgl.resizeCanvasToDisplaySize(gl.canvas);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.useProgram(programInfoDeferred.program);
        twgl.setUniforms(programInfoDeferred, {
            gPosition: fbi.attachments[0],
            gNormal: fbi.attachments[1],
            gAlbedoSpec: fbi.attachments[2],
            u_lightPos: [
                variables.xTranslation,
                variables.yTranslation,
                variables.zTranslation
            ]
        });
        twgl.setBuffersAndAttributes(gl, programInfoDeferred, bufferInfoQuad);
        twgl.drawBufferInfo(gl, bufferInfoQuad);

        requestAnimationFrame(render);
        stats.end();
    }
    requestAnimationFrame(render);
}
