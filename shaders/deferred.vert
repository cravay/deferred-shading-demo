#version 300 es

in vec3 position;

out vec2 texCoordsOut;

void main() {
  texCoordsOut = vec2(position) / 2.0 + 0.5;
  gl_Position = vec4(position, 1.0);
}
