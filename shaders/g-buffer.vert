#version 300 es

uniform mat4 u_perspective;
uniform mat4 u_model;
uniform float u_time;

in vec4 position;
in vec4 normal;

out vec4 positionOut;
out vec4 normalOut;

void main() {
  positionOut = u_model * position;

  mat3 normalMatrix = transpose(inverse(mat3(u_model)));
  normalOut = vec4(normalMatrix * vec3(normal), 1.0);

  gl_Position = u_perspective * u_model * position;
}
