#version 300 es

precision mediump float;

uniform vec3 u_color;

in vec4 positionOut;
in vec4 normalOut;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

void main() {
  gPosition = positionOut.xyz;
  gNormal = normalOut.xyz;
  gAlbedoSpec.rgb = u_color;
  gAlbedoSpec.a = 0.8;
}
