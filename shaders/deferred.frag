#version 300 es

precision highp float;

uniform vec3 u_lightPos;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

in vec2 texCoordsOut;

out vec4 fragColor;

void main() {
  // Retrieve data from g-Buffer
  vec3 FragPos = texture(gPosition, texCoordsOut).rgb;
  vec3 Normal = texture(gNormal, texCoordsOut).rgb;
  vec3 Diffuse = texture(gAlbedoSpec, texCoordsOut).rgb;
  float Specular = texture(gAlbedoSpec, texCoordsOut).a;

  // Properties for lighting (Hardcoded for now)
  vec3 viewPos = vec3(0.0, 0.0, 0.0);
  vec3 lightColor = vec3(1.0, 1.0, 1.0);
  float lightLinear = 0.7;
  float lightQuadratic = 1.0;
  
  // Ambient
  vec3 lighting = Diffuse * 0.2;
  vec3 viewDir  = normalize(viewPos - FragPos);

  // Diffuse
  vec3 lightDir = normalize(u_lightPos - FragPos);
  vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lightColor;

  // Specular
  vec3 halfwayDir = normalize(lightDir + viewDir);
  float spec = pow(max(dot(Normal, halfwayDir), 0.0), 32.0);
  vec3 specular = lightColor * spec * Specular;

  // Attenuation
  float distance = length(u_lightPos - FragPos);
  float attenuation = 1.0 / (1.0 + lightLinear * distance + lightQuadratic * distance * distance);
  
  /*
  Ignore attenuation for now

  diffuse *= attenuation;
  specular *= attenuation;
  */

  lighting = diffuse + specular;

  fragColor = vec4(lighting, 1.0);
}
